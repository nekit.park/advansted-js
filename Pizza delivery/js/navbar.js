let menuBtn = document.getElementsByClassName('header-btn')[0];

menuBtn.onclick = () => {
    let navbarIcon = document.getElementById('navbar-icon');
    navbarIcon.classList.toggle('fa-times');
    
    let navbarMenu = document.getElementsByClassName('navbar')[0];
    navbarMenu.classList.toggle('navbar-active');
}
