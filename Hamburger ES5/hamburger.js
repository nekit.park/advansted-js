function Hamburger(size, stuffing) {
  try{

    if ( arguments.length < 2 || arguments.length > 2 ) {
      throw new Error("Number of parameters passed incorrect");
    }

    if ( size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE) {
      throw new HamburgerException("Hamburger size not set");
    }

    if ( stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_POTATO && stuffing !== Hamburger.STUFFING_SALAD ) {
      throw new HamburgerException("Wrong stuffing name");
    }

    this.size = size;
    this.stuffing = stuffing;
    this.toppings = [];

  } catch(error) {
    console.log(error);
  }
}

function HamburgerException(message) {
    this.name = "Hamburger exeption";
    this.message = message;
}

// Задаем размеры, виды начинок и добавок

Hamburger.SIZE_SMALL = {
  size: "small",
  price: 50,
  calories: 20
};

Hamburger.SIZE_LARGE = {
  size: "large",
  price: 100,
  calories: 40
};

Hamburger.STUFFING_CHEESE = {
  stuff: "cheese",
  price: 10,
  calories: 20
};

Hamburger.STUFFING_SALAD = {
  stuff: "salad",
  price: 20,
  calories: 5
};

Hamburger.STUFFING_POTATO = {
  stuff: "potato",
  price: 15,
  calories: 10
};

Hamburger.TOPPING_MAYO = {
  topping: "mayo",
  price: 20,
  calories: 5
};

Hamburger.TOPPING_SPICE = {
  topping: "spice",
  price: 15,
  calories: 0
};

// Метод добавления ингридиентов в гамбургер

Hamburger.prototype.addTopping = function (topping) {
  try {
    if ( !topping  ||  topping.length < 1)
      throw new HamburgerException("Number of parameters passed incorrect");

      topping.forEach(function(arg) {
        if(arg !== Hamburger.TOPPING_MAYO && arg !== Hamburger.TOPPING_SPICE ) {
          throw new HamburgerException("Wrong stuffing name");
        }
      });

      for ( var i = 0; i < topping.length; i++ ) {
        if (topping[i] == topping[i-1]) throw new HamburgerException("This topping has already been added")
      }
    /*for ( var i = 0; i < topping.length; i++ ) {
      for ( var k = 1; k < topping.length; k++) {
        if ( topping[i] === topping[k] && i !== k )
          throw new HamburgerException("This topping has already been added")
      }
    }*/
    console.log("test");    
    return topping;
  } catch(error) {
    console.log(error);
  }
}
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(hamburger);

console.log(hamburger.addTopping([Hamburger.TOPPING_MAYO,Hamburger.TOPPING_MAYO]));